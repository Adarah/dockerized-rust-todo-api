#![feature(decl_macro)]
#[macro_use]
extern crate rocket;
extern crate openssl;
#[macro_use]
extern crate diesel;

mod models;
mod schema;

use diesel::prelude::*;
use models::{NewTask, NewUser, Task, User};
use rocket_contrib::databases::{database, diesel::PgConnection};
use rocket_contrib::json::Json;

use rocket::routes;

#[database("todo")]
struct DbConn(PgConnection);

#[get("/")]
fn hello() -> &'static str {
    "Hello, world!"
}

#[get("/<name>")]
fn hello_name(name: String) -> String {
    format!("Hello, {}!", name)
}

#[post("/", data = "<new_task>")]
fn save_task(conn: DbConn, new_task: Json<NewTask>) -> Json<Task> {
    use schema::task;
    let result = diesel::insert_into(task::table)
        .values(&*new_task)
        .get_result(&*conn)
        .expect("Error saving task");
    Json(result)
}

#[post("/", data = "<new_user>")]
fn register_user(conn: DbConn, new_user: Json<NewUser>) -> Json<User> {
    use schema::app_user;
    let result = diesel::insert_into(app_user::table)
        .values(&*new_user)
        .get_result(&*conn)
        .expect("Error saving task");
    Json(result)
}

fn main() {
    rocket::ignite()
        .attach(DbConn::fairing())
        .mount("/hello", routes![hello, hello_name])
        .mount("/", routes![save_task])
        .mount("/register", routes![register_user])
        .launch();
}
