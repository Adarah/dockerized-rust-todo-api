table! {
    app_user (id) {
        id -> Int4,
        name -> Varchar,
        password -> Varchar,
    }
}

table! {
    task (id) {
        id -> Int4,
        title -> Text,
        body -> Nullable<Text>,
        due_date -> Nullable<Timestamptz>,
    }
}

joinable!(task -> app_user (id));

allow_tables_to_appear_in_same_query!(app_user, task,);
