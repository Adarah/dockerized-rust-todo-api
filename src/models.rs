use super::schema::{app_user, task};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Queryable, Serialize)]
pub struct Task {
    id: i32,
    title: String,
    body: Option<String>,
    due_data: Option<NaiveDateTime>,
}

#[derive(Insertable, Deserialize)]
#[table_name = "task"]
pub struct NewTask {
    pub title: String,
    pub body: Option<String>,
    pub due_date: Option<NaiveDateTime>,
}

#[derive(Queryable, Serialize)]
pub struct User {
    id: i32,
    name: String,
    password: String,
}

#[derive(Insertable, Deserialize)]
#[table_name = "app_user"]
pub struct NewUser {
    pub name: String,
    pub password: String,
}
