FROM ekidd/rust-musl-builder:nightly-2020-07-12 AS builder

WORKDIR /home/todo_app/

# Avoid having to install/build all dependencies by copying
# the Cargo files and making a dummy src/main.rs
COPY Cargo.toml .
COPY Cargo.lock .
RUN sudo chown -R rust:rust /home/todo_app
RUN mkdir src
RUN echo "fn main() {}" > src/main.rs
RUN cargo test
RUN cargo build --release

# We need to touch our real main.rs file or else docker will use
# the cached one.
COPY . .
RUN sudo touch src/*
RUN sudo touch Rocket.toml

# RUN cargo test
# RUN sudo apt update && sudo apt upgrade -y && sudo apt install -y libdbus-1-dev libsqlite3-dev
RUN cargo build

# Size optimization
# RUN strip target/x86_64-unknown-linux-musl/release/todo_app

# Start building the final image
FROM alpine
WORKDIR /home/todo_app/
COPY --from=builder /home/todo_app/target/x86_64-unknown-linux-musl/debug/todo_app .
COPY --from=builder /home/todo_app/Rocket.toml .
EXPOSE 3000
ENTRYPOINT ["./todo_app"]
